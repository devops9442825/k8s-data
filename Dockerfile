FROM python:3.9-slim

WORKDIR /app

RUN pip install flask

COPY . /app/

EXPOSE 3000

CMD [ "python3","app.py" ]